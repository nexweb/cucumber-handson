## Cucumber란  무엇인가? 

Cucumber는 자동화된 테스트를 실행하기 위한 BDD 프레임워크입니다.
Cucumber는 개발자가 아닌 설계자가 비즈니스 용어로 시나리오를 정의하고, 
데이터 기반, 매개변수화, 실행 제어, 훅, 보고서를 작성지원합니다. 

## Cucumber Terminologies:
Cucumber는 소프트웨어의 동작을 비즈니스 용어로 동작을 설명하여, 비즈니스 가독성을 높여주는 
Gherkin 이라는 도메인 언어를 사용하여 시나리오를 정의합니다.

## Cucumber Keywords
Cucumber에서는 사용하는 키워드들은 아래와 같은 키워드들이 있습니다.
Scenario, Feature, Feature file, Scenario outline, Step Definition 

물리적으로 보면 Featues file와 Step Definition 파일이 있습니다.
Feature 파일에는 BDD를 작성하는 Feature file과 BDD 설계후 동작을 구현하기 위한 Step Definition 영역이 있습니다.   
```cucumber 
Feature file, 
  - Feature, Scenario, Scenario outline, 
Step Definition
```

## Feature and Feature File
Feature는 비즈니스 요구 사항을 표현합니다.
Feature 파일은 1개 이상의 시나리오로 구성된 테스트 묶음 또는 테스트 스위트 역할을 합니다.  

Cucumber에서 Feature 파일은 시나리오를 포함하고, Feature 파일은    
애플리케이션의 특정 영역에 기능의 시나리오는 하나의 Feature 파일로 그룹화됩니다.

Feature 키워드 바로 다음 같은 줄에 텍스트가 Feature 파일의 제목을 정의합니다.  
 Feature 파일에는 시나리오 또는 시나리오 개요가 포함되어야 합니다. Feature 파일의 명명 규칙은 소문자를 사용해야 합니다.  

 > Feature: 신용카드 결제 
 > &emsp;신용 카드 결제 기능을 테스트하려면
 > &emsp;CC 사용자로서
 > &emsp;온라인을 통해 결제를 완료하고 싶습니다.

## Background  
Feature File의 Background 섹션을 사용하면 feature 파일의 모든 시나리오에 공통으로 적용되는 일련의 단계를 지정할 수 있습니다.  Background는 여러 Scenario에 걸쳐서 동일한 Step이 필요한 경우 이것이 반복되는 것을 막고 한데 묶어 표현하는 것을 도와주는 Keyword다. 아래 Background에서 표현되는 것과 같이 Given, And Step을 포함 할 수 있습니다.  

기능 파일의 배경 섹션을 사용하면 파일의 모든 시나리오에 공통으로 적용되는 일련의 단계를 지정할 수 있습니다. 보고서 오류를 반복할 필요 없이 각 시나리오에 대해 토론
단계를 반복해서 설명하는 대신 백그라운드 요소로 옮기면 됩니다. 이렇게 하면 몇 가지 장점이 있습니다:

무슨 뜻인지 보여드리기 위해 기본 거킨 시나리오 요소만 사용하는 기존 시나리오를 배경을 사용하도록 리팩토링하여 가독성을 개선해 보겠습니다. 리팩터링을 시작하기 전의 기능은 다음과 같습니다.  

> Feature: PIN 번호  
> &emsp;새 카드를 발급받는 고객에게는 개인  
  &emsp;개인 식별 번호(PIN)가 제공되며, 이는 시스템에서 무작위로 생성된 시스템에서 무작위로 생성됩니다.
  &emsp;고객이 쉽게 변경할 수 있도록 하려면 새 은행 카드를 소지한 고객은 다음을 수행할 수 있어야 합니다. ATM을 사용하여 비밀번호를 변경할 수 있어야 합니다.

> Scenario: : PIN 변경 성공  
> &emsp;Given 새 카드를 발급받았습니다  
> &emsp;And 카드를 삽입하고 올바른 PIN을 입력합니다. 메뉴에서 "PIN 변경"을 선택합니다  
> &emsp;When 메뉴에서 '비밀번호 변경'을 선택합니다  
> &emsp;And 비밀번호를 9876으로 변경합니다  
> &emsp;Then 시스템이 내 비밀번호를 9876으로 기억해야 합니다  

> Scenario: PIN을 이전과 동일하게 변경을 시도  
> &emsp;Given 새 카드를 발급받았습니다 
> &emsp;And 카드를 삽입하고 올바른 비밀번호를 입력합니다  
> &emsp;When 메뉴에서 '비밀번호 변경'을 선택합니다  
> &emsp;And 비밀번호를 원래 비밀번호 번호로 변경하려고 합니다  
> &emsp;Then 경고 메시지가 표시되어야 합니다  
> &emsp;And 시스템이 내 비밀번호를 변경하지 않아야 합니다

## Scenarios
Cucumber에서 시나리오는 Testcase로 표현됩니다.  
시나리오는 몇계의 스텝을 포함하고, Gherkin 문법을 사용하는 다음의 keywords를 사용합니다.
- **Given**  
- **When**  
- **Then**  
- **But**  

</br>

- **Given**: Given 키워드는 시나리오를 수행되기 전에, 전제 조건이 을 설명합니다. 예를 들면 action을 수행하기 전에 변수값이나 환경 설정들을 수행한다.  
- **When**: When 단계의 목적은 사용자 action을 설명합니다. 테스트에서 원하는 동작을 수행(perform) 하는 부분입니다. API 테스트인 경우 HttpMethod에 해당하는 GET/POST 등이 해당됩니다.    
- **Then**: Then 단계의 목적은 예상되는 기대 결과를 설명합니다. 테스트를 구현할 때는 결과가 정상인 경우 정상/실패 return 값을 반환하는 형식으로 구현합니다  


비즈니스 요구 사항을 지정할 때 여러 가지 전제 조건, 사용자 작업 및 예상 결과가 있는 경우가 있습니다.  

Cucumber에서의 시나리오에 And 및 But 키워드를 사용합니다.  

- **And**: 이전 단계에 추가되는 설명에 사용되며 긍정적인 문장을 나타냅니다  
- **But**: 이전 단계에 추가되는 명으로 부정문을 나타내는 문장에 사용됩니다  

> Scenario: 최소 기한 결제하기   
> 
> &emsp;Given 사용자가 신용카드 결제 페이지에 있습니다  
> &emsp;When 사용자가 모든 세부 정보를 입력하고 최소 금액 옵션을 선택합니다  
> &emsp;And 사용자가 결제 버튼을 클릭합니다  
> &emsp; Then 신용 카드 확인 페이지가 표시됩니다  

> Scenario: 결제 금액 명세서  
> 
> &emsp;Given 사용자는 신용카드 결제 페이지에 있습니다  
  &emsp;When 사용자는 모든 세부 정보를 입력하고 결제 금액 옵션을 선택합니다  
  &emsp;And 사용자가 결제 버튼을 클릭합니다  
  &emsp;Then 신용카드 결제완료 페이지가 표시됩니다  


> Scenario: 결제 금액을 0으로 입력 

> &emsp;Given 사용자는 신용카드 결제 페이지에 있습니다  
  &emsp;When 사용자가 모든 세부 정보를 입력하고 다른 금액을 선택하고 0을 입력합니다.
  &emsp;And 사용자가 결제 버튼을 클릭합니다  
  &emsp;Then 신용 카드 결제완료 페이지가 표시되지 않습니다
  &emsp;But 오류 메시지가 표시됩니다
  
